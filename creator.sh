# create project
curl --location --request POST 'http://webdefl.ru:4440/api/21/projects' \
--header 'X-Rundeck-Auth-Token: EJwQhHDlMyWqgM8klJ4JhwWKIO4urPba' \
--header 'Accept: application/json' \
--header 'Content-Type: application/json' \
--header 'Cookie: JSESSIONID=node01htq2zydohb241tviipp53ty1n25.node0' \
--data-raw '{ 
    "name": "runner"
}'

# create job
curl --location --request POST 'http://webdefl.ru:4440/api/35/project/runner/jobs/import?fileformat=yaml' \
--header 'X-Rundeck-Auth-Token: EJwQhHDlMyWqgM8klJ4JhwWKIO4urPba' \
--header 'Accept: application/json' \
--header 'Content-Type: application/yaml' \
--header 'Cookie: JSESSIONID=node0tnpn5oy72ghun3e7vpp9u1nh7.node0' \
--data-raw '- defaultTab: nodes
  description: run jeknins job
  executionEnabled: true
  id: 3fee6069-c38c-47b7-b2fd-8b9ba2c21a59
  loglevel: INFO
  name: 2jenkins
  nodeFilterEditable: false
  plugins:
    ExecutionLifecycle: null
  scheduleEnabled: true
  sequence:
    commands:
    - script: |-
        curl -u admin:4719f55bcc47496d94b3d081d744750a '\''http://webdefl.ru:8080/job/installer/build?token=foo'\''
    keepgoing: false
    strategy: node-first
  uuid: 3fee6069-c38c-47b7-b2fd-8b9ba2c21a59'

# create 'docker'
curl --location --request POST 'http://webdefl.ru:4440/api/35/project/starter/jobs/import?fileformat=yaml' \
--header 'X-Rundeck-Auth-Token: EJwQhHDlMyWqgM8klJ4JhwWKIO4urPba' \
--header 'Accept: application/json' \
--header 'Content-Type: application/yaml' \
--header 'Cookie: JSESSIONID=node0tnpn5oy72ghun3e7vpp9u1nh7.node0; JSESSIONID=node01qltjr441kzcn17u92nw82m7f422.node0' \
--data-raw '- defaultTab: nodes
  description: docker-installer
  executionEnabled: true
  id: 3fee6069-c38c-47b7-b2fd-8b9ba2c21a59
  loglevel: INFO
  name: docker-installer
  nodeFilterEditable: false
  plugins:
    ExecutionLifecycle: null
  scheduleEnabled: true
  sequence:
    commands:
    - script: |-
        eval `ssh-agent -s`
        ssh-add ~/.ssh/waves2
        ssh root@81.163.28.176 "apt -y update"
        ssh root@81.163.28.176 "apt -y install apt-transport-https ca-certificates curl gnupg-agent software-properties-common"
        ssh root@81.163.28.176 "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - "
        ssh root@81.163.28.176 '\''add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"'\''
        ssh root@81.163.28.176 apt -y update
        ssh root@81.163.28.176 apt -y install docker-ce
        ssh root@81.163.28.176 curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose 
        ssh root@81.163.28.176 chmod +x /usr/local/bin/docker-compose
    keepgoing: false
    strategy: node-first
  uuid: 3fee6069-c38c-47b7-b2fd-8b9ba2c21a54'

# create 'jenkins'
curl --location --request POST 'http://webdefl.ru:4440/api/35/project/starter/jobs/import?fileformat=yaml' \
--header 'X-Rundeck-Auth-Token: EJwQhHDlMyWqgM8klJ4JhwWKIO4urPba' \
--header 'Accept: application/json' \
--header 'Content-Type: application/yaml' \
--header 'Cookie: JSESSIONID=node0tnpn5oy72ghun3e7vpp9u1nh7.node0' \
--data-raw '- defaultTab: nodes
  description: jenkins-installer
  executionEnabled: true
  id: 3fee6069-c38c-47b7-b2fd-8b9ba2c21a59
  loglevel: INFO
  name: jenkins-installer
  nodeFilterEditable: false
  plugins:
    ExecutionLifecycle: null
  scheduleEnabled: true
  sequence:
    commands:
    - script: |-
        eval `ssh-agent -s`
        ssh-add ~/.ssh/waves2
        ssh root@77.223.98.92 "apt -y update"
        ssh root@77.223.98.92 "apt -y install git"
        ssh root@77.223.98.92 "apt -y install openjdk-8-jdk"
        ssh root@77.223.98.92 "wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | sudo apt-key add - "
        ssh root@77.223.98.92 "mkdir /etc/apt/sources.list.d/"
        ssh root@77.223.98.92 "echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list"
        ssh root@77.223.98.92 "apt -y update"
        ssh root@77.223.98.92 "apt -y install jenkins"
        ssh root@77.223.98.92 "cp -r ~/.ssh/ /var/lib/jenkins/"
        ssh root@77.223.98.92 "chown -R jenkins:jenkins /var/lib/jenkins/.ssh/"
        ssh root@77.223.98.92 usermod -aG docker jenkins
        ssh root@77.223.98.92 service jenkins restart
        ssh jenkins@77.223.98.92 docker login --username=waveaccess --password=GsC6qS2ug6CahgJ
    keepgoing: false
    strategy: node-first
  uuid: 3fee6069-c38c-47b7-b2fd-8b9ba2c21a55'

# create nginx
curl --location --request POST 'http://webdefl.ru:4440/api/35/project/starter/jobs/import?fileformat=yaml' \
--header 'X-Rundeck-Auth-Token: EJwQhHDlMyWqgM8klJ4JhwWKIO4urPba' \
--header 'Accept: application/json' \
--header 'Content-Type: application/yaml' \
--header 'Cookie: JSESSIONID=node0tnpn5oy72ghun3e7vpp9u1nh7.node0' \
--data-raw '- defaultTab: nodes
  description: nginx-installer
  executionEnabled: true
  id: 3fee6069-c38c-47b7-b2fd-8b9ba2c21a59
  loglevel: INFO
  name: nginx-installer
  nodeFilterEditable: false
  plugins:
    ExecutionLifecycle: null
  scheduleEnabled: true
  sequence:
    commands:
    - script: |-
        eval `ssh-agent -s`
        ssh-add ~/.ssh/waves2
        ssh root@81.163.28.176 "apt -y update"
        ssh root@81.163.28.176 "apt -y install nginx"
    keepgoing: false
    strategy: node-first
  uuid: 3fee6069-c38c-47b7-b2fd-8b9ba2c21a56'  

# create postgres
scp docker-compose-postgres.yml root@188.246.227.198:/
curl --location --request POST 'http://webdefl.ru:4440/api/35/project/starter/jobs/import?fileformat=yaml' \
--header 'X-Rundeck-Auth-Token: EJwQhHDlMyWqgM8klJ4JhwWKIO4urPba' \
--header 'Accept: application/json' \
--header 'Content-Type: application/yaml' \
--header 'Cookie: JSESSIONID=node0tnpn5oy72ghun3e7vpp9u1nh7.node0' \
--data-raw '- defaultTab: nodes
  description: postgres-installer
  executionEnabled: true
  id: 3fee6069-c38c-47b7-b2fd-8b9ba2c21a59
  loglevel: INFO
  name: postgres-installer
  nodeFilterEditable: false
  plugins:
    ExecutionLifecycle: null
  scheduleEnabled: true
  sequence:
    commands:
    - script: |-
        eval `ssh-agent -s`
        ssh-add ~/.ssh/waves2
        scp docker-compose-postgres.yml root@81.163.28.176:~/
        ssh root@81.163.28.176 "docker-compose -f docker-compose-postgres.yml up -d"
    keepgoing: false
    strategy: node-first
  uuid: 3fee6069-c38c-47b7-b2fd-8b9ba2c21a57'  

# create mysql
scp docker-compose-mysql.yml root@188.246.227.198:/
curl --location --request POST 'http://webdefl.ru:4440/api/35/project/starter/jobs/import?fileformat=yaml' \
--header 'X-Rundeck-Auth-Token: EJwQhHDlMyWqgM8klJ4JhwWKIO4urPba' \
--header 'Accept: application/json' \
--header 'Content-Type: application/yaml' \
--header 'Cookie: JSESSIONID=node0tnpn5oy72ghun3e7vpp9u1nh7.node0' \
--data-raw '- defaultTab: nodes
  description: mysql-installer
  executionEnabled: true
  id: 3fee6069-c38c-47b7-b2fd-8b9ba2c21a59
  loglevel: INFO
  name: mysql-installer
  nodeFilterEditable: false
  plugins:
    ExecutionLifecycle: null
  scheduleEnabled: true
  sequence:
    commands:
    - script: |-
        eval `ssh-agent -s`
        ssh-add ~/.ssh/waves2
        scp docker-compose-mysql.yml root@81.163.28.176:~/
        ssh root@81.163.28.176 "docker-compose -f docker-compose-mysql.yml up -d"
    keepgoing: false
    strategy: node-first
  uuid: 3fee6069-c38c-47b7-b2fd-8b9ba2c21a40'  

# create teamcity
scp docker-compose-teamcity.yml root@188.246.227.198:/
curl --location --request POST 'http://webdefl.ru:4440/api/35/project/starter/jobs/import?fileformat=yaml' \
--header 'X-Rundeck-Auth-Token: EJwQhHDlMyWqgM8klJ4JhwWKIO4urPba' \
--header 'Accept: application/json' \
--header 'Content-Type: application/yaml' \
--header 'Cookie: JSESSIONID=node0tnpn5oy72ghun3e7vpp9u1nh7.node0' \
--data-raw '- defaultTab: nodes
  description: teamcity-installer
  executionEnabled: true
  id: 3fee6069-c38c-47b7-b2fd-8b9ba2c21a59
  loglevel: INFO
  name: teamcity-installer
  nodeFilterEditable: false
  plugins:
    ExecutionLifecycle: null
  scheduleEnabled: true
  sequence:
    commands:
    - script: |-
        eval `ssh-agent -s`
        ssh-add ~/.ssh/waves2
        scp docker-compose-teamcity.yml root@81.163.28.176:~/
        ssh root@81.163.28.176 "docker-compose -f docker-compose-teamcity.yml up -d"
    keepgoing: false
    strategy: node-first
  uuid: 3fee6069-c38c-47b7-b2fd-8b9ba2c21a41'  


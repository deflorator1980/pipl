ssh root@79.143.29.116 apt -y update
ssh root@79.143.29.116 apt -y install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
ssh root@79.143.29.116 'curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -'
ssh root@79.143.29.116 'add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"'
ssh root@79.143.29.116 apt -y update
ssh root@79.143.29.116 apt -y install docker-ce
ssh root@79.143.29.116 'curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose'
ssh root@79.143.29.116 chmod +x /usr/local/bin/docker-compose

